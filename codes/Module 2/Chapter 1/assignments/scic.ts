var num1:HTMLInputElement=<HTMLInputElement>document.getElementById("num1");
var num2:HTMLInputElement=<HTMLInputElement>document.getElementById("num2");
var answer:HTMLInputElement=<HTMLInputElement>document.getElementById("answer");

function add()
{
    var badd:number=parseFloat(num1.value)+parseFloat(num2.value);
    answer.value=badd.toString();
}
function sub(){
    var bsub:number=parseFloat(num1.value)-parseFloat(num2.value);
    answer.value=bsub.toString();

}
function mul(){
    var bmul:number=parseFloat(num1.value)*parseFloat(num2.value);
    answer.value=bmul.toString();

}
function div(){
    var bdiv:number=parseFloat(num1.value)/parseFloat(num2.value);
    answer.value=bdiv.toString();

}
function sine(){
    let a:number=Math.PI/180*parseFloat(num1.value);
    var bsine:number=Math.sin(a);
    answer.value=bsine.toString();
}
function cosine(){
    let a:number=Math.PI/180*parseFloat(num1.value);
    var bcos:number=Math.cos(a);
    answer.value=bcos.toString();
}

function tan(){
    let a:number=Math.PI/180*parseFloat(num1.value);
    var btan:number=Math.tan(a);
    answer.value=btan.toString();
}
function sqrt(){
    var bsqrt:number=Math.sqrt(parseFloat(num1.value));
    answer.value=bsqrt.toString();
}

function power(){
    var bpower:number=Math.pow(parseFloat(num1.value), parseFloat(num2.value));
    answer.value=bpower.toString();
}
