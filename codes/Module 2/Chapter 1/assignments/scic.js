var num1 = document.getElementById("num1");
var num2 = document.getElementById("num2");
var answer = document.getElementById("answer");
function add() {
    var badd = parseFloat(num1.value) + parseFloat(num2.value);
    answer.value = badd.toString();
}
function sub() {
    var bsub = parseFloat(num1.value) - parseFloat(num2.value);
    answer.value = bsub.toString();
}
function mul() {
    var bmul = parseFloat(num1.value) * parseFloat(num2.value);
    answer.value = bmul.toString();
}
function div() {
    var bdiv = parseFloat(num1.value) / parseFloat(num2.value);
    answer.value = bdiv.toString();
}
function sine() {
    let a = Math.PI / 180 * parseFloat(num1.value);
    var bsine = Math.sin(a);
    answer.value = bsine.toString();
}
function cosine() {
    let a = Math.PI / 180 * parseFloat(num1.value);
    var bcos = Math.cos(a);
    answer.value = bcos.toString();
}
function tan() {
    let a = Math.PI / 180 * parseFloat(num1.value);
    var btan = Math.tan(a);
    answer.value = btan.toString();
}
function sqrt() {
    var bsqrt = Math.sqrt(parseFloat(num1.value));
    answer.value = bsqrt.toString();
}
function power() {
    var bpower = Math.pow(parseFloat(num1.value), parseFloat(num2.value));
    answer.value = bpower.toString();
}
//# sourceMappingURL=scic.js.map