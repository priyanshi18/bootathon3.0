function check3() {
    var a = document.getElementById("num");
    var b = document.getElementById("real");
    var c = document.getElementById("img");
    var data = a.value;
    var real;
    var imaginary;
    var i = data.indexOf("+");
    var j = data.indexOf("-");
    if (i != -1) {
        real = +data.substring(0, i);
        imaginary = +data.substring(i + 1, data.length - 1);
        b.value = "Real part is :" + real;
        c.value = "Complex Part is :" + imaginary;
    }
    else if (j != -1) {
        real = +data.substring(0, j);
        imaginary = +data.substring(j + 1, data.length - 1);
        b.value = "Real part is: " + real;
        c.value = "Complex Part is :" + imaginary;
    }
    else {
        real = +data.substring(0, data.length);
        b.value = "Real part :" + real;
        c.value = "Complex part: 0 ";
    }
}
//# sourceMappingURL=ass3.js.map