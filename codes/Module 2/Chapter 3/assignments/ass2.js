function check1() {
    var n1 = document.getElementById("t1");
    var n2 = document.getElementById("t2");
    var n3 = document.getElementById("t3");
    var a = +n1.value;
    var b = +n2.value;
    var c = +n3.value;
    console.log(a);
    console.log(b);
    console.log(c);
    if (a == b && b == c) {
        alert("It is equilateral triangle");
    }
    else if (a != b && b != c && c != a) {
        if ((a * a + b * b == c * c) || (c * c + b * b == a * a) || (a * a + c * c == b * b)) {
            alert("Right Angled Triangle");
        }
        else {
            alert("It is scalene triangle");
        }
    }
    else if ((a == b && a != c) || (b == c && b != a) || (c == a && c != b)) {
        alert("It is isoceles triangle");
    }
    else {
        alert("Error in your entered sides");
    }
}
//# sourceMappingURL=ass2.js.map